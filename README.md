# Description #

This is used to setup the MEAN stack for the chat project.

* In order for all this to work you'll need: docker-compose and docker to be both installed on the machine.
	

	
# USAGE #

	Before running this command, make sure the following ports are not used by another application: 3000, 27017, 4200.
	If you want to stop the current application which is listening on one port 
	or just want to make sure that nothing is listening on a specific port,
	use this command: sudo kill `sudo lsof -t -i:port_number`
	or this command: sudo kill $(sudo lsof -t -i:port_number)
	replace port_number with the port number which you want to release
	
	You can use any of those two commands, if run the command and nothing is listening on the port that you set, you 
	will receive the kill options which is fine, you can ignore that.
	
sh build.sh 

* This will clone the repositories and start docker containers.
* It will open the nodejs server on port 3000, the mongo database on port 27017 and the angular client on port 4200
* to access the application go to http://localhost:4200


### If you need to reset everything in the chat project: ###


sh reset.sh 


* this will delete the folders and containers
* the next step after a reset is sh build.sh
	
### If for some reason one ore more docker containers stopped: ###


sh run.sh 


* this will start all the containers